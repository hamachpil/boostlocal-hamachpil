FROM python:3.10

RUN mkdir /app
RUN mkdir /data

COPY . /app
COPY env.docker /app/.env

WORKDIR /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD}

RUN pip3 install poetry
RUN poetry config virtualenvs.create false

# This is due to a bug in poetry that may be eliminated in future versions
# But currently it will wipe out packages poetry itself needs (2021-12-07)
#RUN poetry install --no-dev
RUN poetry install
CMD [ "poetry", "run" "main:main" ]
