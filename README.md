
# Setup

This bot ultimately only needs the API_BASE_URL and ACCESS_TOKEN environment variables to be set, but in order to get the token, a bot must be registered on the Mastodon instance from the website. There, the capabilities it has can be selected, and then a client ID can be set, along with a client secret.

Once that's done, you can log into the Mastodon instance as per the Mastodon.py docs and get the access token, which will be saved to the `to_file`.

```
from mastodon import Mastodon

mastodon = Mastodon(
    client_id = 'pytooter_clientcred.secret',
    api_base_url = 'https://mastodon.social'
)
mastodon.log_in(
    'my_login_email@example.com',
    'incrediblygoodpassword',
    to_file = 'pytooter_usercred.secret'
)
```
From there, all that's needed is the API_BASE_URL and the ACCESS_TOKEN, and those can be fed into the script by environment variable.

You can also get the keys from the application's profile page on the Mastodon instance.


