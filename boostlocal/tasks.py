import logging
from .settings import huey, mstdn, my_account_id

# Keep trying every minute for about four days
@huey.task(retries=99, retry_delay=60)
def boost_toot(toot):
    """Boost the toot"""
    # Check to be sure this isn't from us
    if toot['account']['id'] == my_account_id:
        logging.debug(f"Ignoring toot {toot['id']} as it comes from us")
        return
    logging.info(f"Reblogging toot {toot['id']}")
    mstdn.status_reblog(toot)
