from os import environ as env
from huey import SqliteHuey
from mastodon import Mastodon

HUEY_SQLITE_DB = env['HUEY_SQLITE_DB']
API_BASE_URL = env['API_BASE_URL']
ACCESS_TOKEN = env['ACCESS_TOKEN']
LAST_SEEN = env.get('LAST_SEEN')

huey = SqliteHuey(filename=HUEY_SQLITE_DB)

mstdn = Mastodon(api_base_url=API_BASE_URL, access_token=ACCESS_TOKEN)
my_account_id = mstdn.me()['id']
