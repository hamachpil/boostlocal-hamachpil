"""Boost all toots on a Mastodon instance"""
from sys import stdout
from pathlib import Path
from mastodon import Mastodon, StreamListener
import logging
from .settings import huey, mstdn, my_account_id, LAST_SEEN
from .tasks import boost_toot

logging.getLogger().setLevel(logging.DEBUG)

logging.info("Loading config")

# Load the last seen toot
LAST_TOOT = None

lastseen_path = Path(LAST_SEEN)
if lastseen_path.exists():
    logging.info(f"Loading lastseen file {LAST_SEEN}")
    with open(LAST_SEEN, 'r') as fd:
        content = fd.read().strip()
        if content:
            LAST_TOOT = int(content)
else:
    logging.info(f"No lastseen file {LAST_SEEN}")

def update_lastseen(toot_id):
    """Update the last_seen file with the current toot"""
    logging.debug(f"Updating lastseen with {toot_id}")
    with open(LAST_SEEN, 'w') as fd:
        fd.write(str(toot_id))

def toot_booster(toot):
    """Boost the toot"""
    boost_toot(toot)
    update_lastseen(toot['id'])

class BoostListener(StreamListener):
    """A StreamListener that only boosts toots"""
    def on_update(self, status):
        toot_booster(status)

def main():
    if LAST_TOOT:
        # Catch up to new toots
        logging.info(f"Catching up since toot {LAST_TOOT}")
        for status in mstdn.timeline_local(since_id=LAST_TOOT):
            toot_booster(status)

        logging.info("Creating Listener")
        # Create the stream listener
        while True:
            mstdn.stream_local(BoostListener())
